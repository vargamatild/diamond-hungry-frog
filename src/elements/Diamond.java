package elements;
import java.awt.*;

public class Diamond extends Element {
    public Diamond(int x, int y) {
        super(x, y);
        initDiamond();
    }

    public void initDiamond() {
        Image image = Toolkit.getDefaultToolkit().getImage("resources/images/diamond.png");
        setImage(image);
    }
}

package elements;
import java.awt.*;

public class Element {
    private int x;
    private int y;
    private int width = 40;
    private int height = 20;
    private Image image;

    public Element(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Rectangle getRectangle() {
        return new Rectangle(x, y, width, height);
    }

    public Rectangle getRectangleRight() {
        return new Rectangle(x + width - 2, y + 2, 2, height - 4);
    }

    public Rectangle getRectangleLeft() {
        return new Rectangle(x, y + 2, 2, height - 4);
    }

    public Rectangle getRectangleBottom() {
        return new Rectangle(x + 2, y + height - 2, width - 4, 2);
    }

    public Rectangle getRectangleTop() {
        return new Rectangle(x + 2, y, width - 4, 2);
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
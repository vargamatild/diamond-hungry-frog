package elements;
import java.awt.*;

public class Flag extends Element {
    public Flag(int x, int y) {
        super(x, y);
        initFlag();
    }

    public void initFlag() {
        Image image = Toolkit.getDefaultToolkit().getImage("resources/images/flag.png");
        setImage(image);
    }
}

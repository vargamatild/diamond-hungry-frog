package elements;

import spriteSheet.Sprite;
import spriteSheet.SpriteSheet;

public class Player extends Element {
    private int direction = 1;
    private float animation = 1;
    private Sprite sprite;

    public Player(int x, int y) {
        super(x, y);
        initPlayer();
    }

    public void initPlayer() {
        sprite = new Sprite(new SpriteSheet("resources/images", "player.png"), (int)animation, direction, 32, 32);
        setImage(sprite.getBufferedImage());
        setWidth(30);
    }

    public void move (int x, int y) {
        setX(getX() + x);
        setY(getY() + y);
    }

    public void setDirection(int direction) {
        this.direction = direction;
        initPlayer();
    }

    public void setAnimation(float animation) {
        this.animation = animation;
        initPlayer();
    }
}

package elements;
import spriteSheet.Sprite;
import spriteSheet.SpriteSheet;

public class Spike extends Element {
    private Sprite sprite;

    public Spike(int x, int y) {
        super(x, y);
        initSpike();
    }

    public void initSpike() {
        sprite = new Sprite(new SpriteSheet("resources/images", "tileset.png"), 3,14, 16, 8);
        setImage(sprite.getBufferedImage());
    }
}
package elements;

import spriteSheet.SpriteSheet;
import spriteSheet.Sprite;

public class SpikeLeft extends Spike {
    private Sprite sprite;

    public SpikeLeft(int x, int y) {
        super(x, y);
        initSpike();
    }

    public void initSpike() {
        sprite = new Sprite(new SpriteSheet("resources/images", "tileset.png"), 2,17, 16, 8);
        setImage(sprite.getBufferedImage());
    }
}

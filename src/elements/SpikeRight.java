package elements;

import spriteSheet.Sprite;
import spriteSheet.SpriteSheet;

public class SpikeRight extends Spike {
    private Sprite sprite;

    public SpikeRight(int x, int y) {
        super(x, y);
        initSpike();
    }

    public void initSpike() {
        sprite = new Sprite(new SpriteSheet("resources/images", "tileset2.png"), 1,18, 16, 8);
        setImage(sprite.getBufferedImage());
    }
}

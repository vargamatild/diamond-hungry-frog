package elements;

import spriteSheet.Sprite;
import spriteSheet.SpriteSheet;

public class Wall extends Element {
    private Sprite sprite;
    private final int[] indexX = new int[4];
    private final int[] indexY = new int[4];
    private int actualIndexX;
    private int actualIndexY;

    public Wall(int x, int y) {
        super(x, y);
        initIndexArrays();
    }

    public void initWall() {
        sprite = new Sprite(new SpriteSheet("resources/images", "tileset.png"), actualIndexX, actualIndexY, 16, 8);
        setImage(sprite.getBufferedImage());
    }

    public void setWallStyle(int x) {
        actualIndexX = indexX[x];
        actualIndexY = indexY[x];
        initWall();
    }

    public void initIndexArrays() {
        indexX[0] = 4;
        indexY[0] = 7;

        indexX[1] = 1;
        indexY[1] = 1;

        indexX[2] = 6;
        indexY[2] = 19;

        indexX[3] = 2;
        indexY[3] = 15;
    }
}
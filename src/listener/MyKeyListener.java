package listener;

import main.PlayGame;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MyKeyListener implements KeyListener {
    private final boolean[] keys = new boolean[256];

    public MyKeyListener(PlayGame playGame) {
        playGame.getWindow().addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }

    public void setReleaseAllKeys () {
        for (int i = 0; i < 256; i++) {
            keys[i] = false;
        }
    }

    public boolean isKey(int keyCode) {
        return keys[keyCode];
    }
}

package main;

import elements.*;
import listener.MyKeyListener;
import views.Board;
import views.LevelPanel;
import views.StartPanel;
import views.Window;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.String.valueOf;

public class PlayGame implements Runnable {
    private final int width = 695;
    private final int height = 695;
    private final double updateCap = 1.0 / 60.0;
    private final float fallSpeed = 3;
    private final int offset = 2;
    private boolean running = false;
    private Thread thread;
    private Window window;
    private MyKeyListener myKeyListener;
    private Player player;
    private Flag flag;
    private float fallDistance = 0;
    private boolean ground = false;
    private ArrayList<Wall> walls;
    private ArrayList<Spike> spikes;
    private ArrayList<Diamond> diamonds;
    private Boolean ok = false;
    private Board board;
    private boolean dead = false;
    private String levelNr;
    private float animationRight = 1;
    private float animationLeft = 12;
    private int total = 0;
    private int unlocked = 1;
    private StartPanel startPanel;
    private LevelPanel levelPanel;

    public PlayGame() {
        player = new Player(0, 0);
        flag = new Flag(0, 0);
        walls = new ArrayList<>();
        spikes = new ArrayList<>();
        diamonds = new ArrayList<>();
    }

    public static void main(String[] args) {
        PlayGame playGame = new PlayGame();
        playGame.start();
    }

    // initializations
    public void start() {
        window = new Window(new JPanel());
        myKeyListener = new MyKeyListener(this);
        levelPanel = new LevelPanel(this);
        startPanel = new StartPanel(this);
        window.setPanel(startPanel);
        window.setStartPanel(startPanel);
        window.setLevelPanel(levelPanel);
        thread = new Thread(this);
        thread.run();
    }

    // game loop: render and update
    @Override
    public void run() {
        running = true;
        double firstTime = 0;
        double lastTime = System.nanoTime() / 1000000000.0;
        double passedTime = 0;
        double unprocessedTime = 0;
        boolean render = false;
        double frameTime = 0;
        int frames = 0;
        int fps = 0;

        while (running) {
            render = false;
            firstTime = System.nanoTime() / 1000000000.0;
            passedTime = firstTime - lastTime;
            lastTime = firstTime;

            unprocessedTime += passedTime;
            frameTime += passedTime;

            while (unprocessedTime >= updateCap) {
                unprocessedTime -= updateCap;
                render = true;

                //update frame
                // when a Board is created and a level is on the screen
                if (ok) {
                    update();
                }

                if (frameTime >= 1.0) {
                    frameTime = 0;
                    fps = frames;
                    frames = 0;
                    System.out.println(fps);
                }
            }

            if (render) {
                window.repaint();
                window.revalidate();
                frames++;
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // reset the board elements
    public void reset() {
        walls = new ArrayList<>();
        spikes = new ArrayList<>();
        diamonds = new ArrayList<>();
        flag = new Flag(0, 0);
        player = new Player(-43, -43);
    }

    // moving and collision checking
    public void update() {
        fallDistance += 0.15 * fallSpeed;

        player.move(0, (int) fallDistance);

        checkPressedKeysForPlayer();

        checkCollisionWithSpikes();

        if (dead) {
            ifDead();

        } else {
            checkWallCollision();

            checkDiamondCollision();

            checkFlagCollision();
        }
    }

    // player's movement and player's animation
    public void checkPressedKeysForPlayer() {
        if (myKeyListener.isKey(KeyEvent.VK_LEFT)) {
            player.move(-2, 0);
            player.setDirection(2);
            animationLeft -= 0.25 * 3;
            if (animationLeft <= 1) {
                animationLeft = 12;
            }
            player.setAnimation(animationLeft);
        }

        if (myKeyListener.isKey(KeyEvent.VK_RIGHT)) {
            player.move(2, 0);
            player.setDirection(1);
            animationRight += 0.25 * 3;
            if (animationRight >= 12) {
                animationRight = 1;
            }
            player.setAnimation(animationRight);
        }

        if (myKeyListener.isKey(KeyEvent.VK_UP) && ground) {
            fallDistance = -9;
            ground = false;
            player.move(0, (int) fallDistance);
            animationRight = 1;
            player.setAnimation(animationRight);
        }
    }

    // collision checking with obstacles
    public void checkCollisionWithSpikes() {
        for (Spike element : spikes) {
            if (player.getRectangleBottom().intersects(element.getRectangle())) {
                dead = true;
                break;
            }

            if (player.getRectangleTop().intersects(element.getRectangle())) {
                dead = true;
                break;
            }

            if (player.getRectangleLeft().intersects(element.getRectangle())) {
                dead = true;
                break;
            }

            if (player.getRectangleRight().intersects(element.getRectangle())) {
                dead = true;
                break;
            }
        }
    }

    // if the player has collided with an obstacle, he can choose: restart the level or not
    public void ifDead() {
        ok = false;
        dead = false;
        myKeyListener.setReleaseAllKeys();
        reset();

        int n = JOptionPane.showConfirmDialog(null, "You are dead. Do you want to restart the level?", "Info!", JOptionPane.YES_NO_OPTION);

        if (n == JOptionPane.NO_OPTION || n == JOptionPane.CLOSED_OPTION) {
            window.setPanel(levelPanel);
            myKeyListener.setReleaseAllKeys();
            levelPanel.requestFocus();
        } else if (n == JOptionPane.YES_OPTION) {
            board = new Board(width, height, player, flag, walls, spikes, diamonds, levelNr);
            window.setPanel(board);
            myKeyListener.setReleaseAllKeys();
            window.requestFocus();
            ok = true;
            fallDistance = 0;
        }
    }


    // collision with walls
    public void checkWallCollision() {
        for (Wall element : walls) {
            if (player.getRectangleBottom().intersects(element.getRectangle())) {
                fallDistance = 0;
                player.setY(element.getY() - player.getHeight());
                ground = true;
                break;
            }

            if (player.getRectangleTop().intersects(element.getRectangle())) {
                player.setY(element.getY() + player.getHeight());
                ground = false;
                break;
            }

            if (player.getRectangleLeft().intersects(element.getRectangle())) {
                player.setX(element.getX() + player.getWidth() + 10 + offset);
                break;
            }

            if (player.getRectangleRight().intersects(element.getRectangle())) {
                player.setX(element.getX() - player.getWidth() - offset);
                break;
            }
        }
    }

    // collision with diamonds: the player picks it up and the points will be updated
    public void checkDiamondCollision() {
        for (Diamond diamond : diamonds) {
            if (player.getRectangleBottom().intersects(diamond.getRectangle()) ||
                    player.getRectangleTop().intersects(diamond.getRectangle()) ||
                    player.getRectangleLeft().intersects(diamond.getRectangle()) ||
                    player.getRectangleRight().intersects(diamond.getRectangle())) {
                Sound sound = new Sound("resources/sounds/score.wav");
                sound.play();
                board.setScoreText(valueOf(Integer.parseInt(board.getScoreText()) + 100));
                diamonds.remove(diamond);
                break;
            }
        }
    }


    // arriving to the finish flag
    public void checkFlagCollision() {
        if (player.getRectangleBottom().intersects(flag.getRectangle()) ||
                player.getRectangleTop().intersects(flag.getRectangle()) ||
                player.getRectangleLeft().intersects(flag.getRectangle()) ||
                player.getRectangleRight().intersects(flag.getRectangle())) {
            int n = JOptionPane.showConfirmDialog(null, "Congratulation! Your accumulated " + Integer.parseInt(board.getScoreText()) / 100 + " diamonds.", "Info!", JOptionPane.DEFAULT_OPTION);
            if (n == JOptionPane.OK_OPTION || n == JOptionPane.CLOSED_OPTION) {
                total += Integer.parseInt(board.getScoreText()) / 100;
                if (total >= 5) {
                    unlocked = total / 5 + 1;
                    Arrays.stream(levelPanel.getLevelButton()).filter(s -> Integer.parseInt(s.getText()) <= unlocked).forEach(s -> s.setBackground(new Color(137, 207, 240)));
                }
                levelPanel.getDiamondsAccumulated().setText("Diamonds in total: " + total);
                window.setPanel(levelPanel);
                myKeyListener.setReleaseAllKeys();
                levelPanel.requestFocus();
                ok = false;
                dead = false;
                reset();
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Window getWindow() {
        return window;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public MyKeyListener getMyKeyListener() {
        return myKeyListener;
    }

    public LevelPanel getLevelPanel() {
        return levelPanel;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getUnlocked() {
        return unlocked;
    }

    public void setUnlocked(int unlocked) {
        this.unlocked = unlocked;
    }

    public Player getPlayer() {
        return player;
    }

    public Flag getFlag() {
        return flag;
    }

    public ArrayList<Wall> getWalls() {
        return walls;
    }

    public ArrayList<Spike> getSpikes() {
        return spikes;
    }

    public ArrayList<Diamond> getDiamonds() {
        return diamonds;
    }

    public void setLevelNr(String levelNr) {
        this.levelNr = levelNr;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public void setLevelPanel(LevelPanel levelPanel) {
        this.levelPanel = levelPanel;
    }
}

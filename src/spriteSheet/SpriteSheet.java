package spriteSheet;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SpriteSheet {
    private BufferedImage sheet;

    public SpriteSheet(String parent, String child) {
        try {
            sheet = ImageIO.read(new File(parent, child));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public BufferedImage getSprite (int x, int y, int width, int height) {
            return sheet.getSubimage(x * width - width, y * height - height, width, height);
    }
}

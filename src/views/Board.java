package views;

import elements.*;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Board extends JPanel {
    private final int width;
    private final int height;
    private final Player player;
    private final Flag flag;
    private final ArrayList<Wall> walls;
    private final ArrayList<Spike> spikes;
    private final ArrayList<Diamond> diamonds;
    private String fileName = "levels/level";
    private final String level;
    private final JLabel score;
    private final JLabel label;
    private Font font;

    public Board(int width, int height, Player player, Flag flag, ArrayList<Wall> walls, ArrayList<Spike> spikes, ArrayList<Diamond> diamonds, String level) {
        this.width = width;
        this.height = height;
        this.player = player;
        this.flag = flag;
        this.walls = walls;
        this.spikes = spikes;
        this.diamonds = diamonds;
        this.level = level;
        this.score = new JLabel();

        score.setBounds(180, 50, 50, 20);
        score.setText("0");
        label = new JLabel("Actual score: ");
        label.setBounds(40, 50, 150, 20);

        try {
            font = Font.createFont(Font.TRUETYPE_FONT, new File("resources/fonts/FFF_Tusj.ttf")).deriveFont(18f);
            GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
            graphicsEnvironment.registerFont(font);
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }

        label.setFont(font);
        score.setFont(font);
        setLayout(null);

        initBoard();
    }

    // level building
    public void initBoard() {
        Diamond diamond;
        Wall wall;
        Spike spike;
        fileName += level + ".txt";
        Random random = new Random();
        int rand = random.nextInt(4);

        int space = 40;
        int x = -space / 2;
        int y = 0;
        try {
            Scanner scanner = new Scanner(new File(fileName));
            while (scanner.hasNextLine()){
                String line = scanner.nextLine();
                for (int i = 0; i < line.length(); i++) {
                    switch (line.charAt(i)) {
                        case '#' -> {
                            wall = new Wall(x, y);
                            wall.setWallStyle(rand);
                            walls.add(wall);
                            x += space;
                        }
                        case 'd' -> {
                            diamond = new Diamond(x, y);
                            diamonds.add(diamond);
                            x += space;
                        }
                        case ' ' -> x += space;
                        case 'z' -> {
                            spike = new Spike(x, y);
                            spikes.add(spike);
                            x += space;
                        }
                        case '@' -> {
                            player.setX(x);
                            player.setY(y);
                            x += space;
                        }
                        case 'a' -> {
                            flag.setX(x);
                            flag.setY(y);
                            x += space;
                        }
                        case 'l' -> {
                            spike = new SpikeLeft(0, y);
                            spikes.add(spike);
                            x += space;
                        }
                        case 'r' -> {
                            spike = new SpikeRight(660, y);
                            spikes.add(spike);
                            x += space;
                        }
                    }
                }

                x = -space / 2;
                y += space / 2;
            }

            repaint();
            revalidate();

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Image img = Toolkit.getDefaultToolkit().getImage("resources/images/bg.jpg");
        g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), null);

        buildBoard(g);
    }

    // level painting(showing)
    public void buildBoard(Graphics g) {
        ArrayList<Element> world = new ArrayList<>();
        world.addAll(walls);
        world.addAll(spikes);
        world.addAll(diamonds);

        world.stream().forEach(e -> g.drawImage(e.getImage(), e.getX(), e.getY(), 40, 20, this));

        g.drawImage(flag.getImage(), flag.getX(), flag.getY() - 20, 40, 40, this);
        g.drawImage(player.getImage(), player.getX(), player.getY() - 10, 32, 32, this);

        add(label);
        add(score);
    }

    public String getScoreText() {
        return score.getText();
    }

    public void setScoreText(String text) {
        this.score.setText(text);
    }
}

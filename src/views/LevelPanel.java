package views;

import main.PlayGame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import static java.lang.String.valueOf;

public class LevelPanel extends JPanel {
    private Font font;
    private final Image img;
    private final int width;
    private final int height;
    private final JButton[] levelButton = new JButton[6];
    private final JPanel panel = this;
    private final JLabel diamondsAccumulated;
    private Board board;
    private final PlayGame playGame;

    public JLabel getDiamondsAccumulated() {
        return diamondsAccumulated;
    }

    public LevelPanel(PlayGame playGame) {
        this.playGame = playGame;
        this.img = new ImageIcon("resources/images/bg.jpg").getImage();
        this.width = playGame.getWidth();
        this.height = playGame.getHeight();
        diamondsAccumulated = new JLabel();
        setLayout(new FlowLayout());

        try {
            font = Font.createFont(Font.TRUETYPE_FONT, new File("resources/fonts/FFF_Tusj.ttf")).deriveFont(36f);
            GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
            graphicsEnvironment.registerFont(font);
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }

        diamondsAccumulated.setText("Diamonds in total: " + playGame.getTotal());
        diamondsAccumulated.setFont(font);
        diamondsAccumulated.setBounds(100, 400, 500, 81);

        initLevelButtons();

        add(diamondsAccumulated);

    }

    // initialisation of the level buttons
    public void initLevelButtons () {
        for (int i = 0; i < levelButton.length; i++) {
            levelButton[i] = new JButton(valueOf(i + 1));
            levelButton[i].setForeground(Color.BLACK);
            levelButton[i].setBackground(new Color(179, 120, 73));
            if (i == 0) {
                levelButton[i].setBackground(new Color(137, 207, 240));
            }
            levelButton[i].setFont(font);
            levelButton[i].setFocusable(false);
            add(levelButton[i]);
            repaint();

            int finalI = i;

            levelButton[i].addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (finalI + 1 <= playGame.getUnlocked()) {
                        playGame.reset();
                        board = new Board(width, height, playGame.getPlayer(), playGame.getFlag(), playGame.getWalls(), playGame.getSpikes(), playGame.getDiamonds(), levelButton[finalI].getText());
                        playGame.setBoard(board);
                        playGame.setLevelNr(levelButton[finalI].getText());
                        playGame.getWindow().setPanel(board);
                        playGame.getWindow().setScoreText("0");
                        playGame.getWindow().requestFocus();
                        playGame.setOk(true);
                    } else {
                        int n = JOptionPane.showConfirmDialog(null, "Try to collect more diamonds to unlock levels!", "Info!", JOptionPane.DEFAULT_OPTION);
                        if (n == JOptionPane.OK_OPTION || n == JOptionPane.CLOSED_OPTION) {
                            playGame.reset();
                            playGame.getWindow().setPanel(panel);
                        }
                    }
                }
            });
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, width, height, null);
    }

    public JButton[] getLevelButton() {
        return levelButton;
    }
}

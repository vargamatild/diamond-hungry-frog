package views;

import main.PlayGame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class StartPanel extends JPanel {
    private final JButton startButton;
    private final JButton exitButton;
    private final JButton continueButton;
    private final Image img;
    private final int width;
    private final int height;
    private LevelPanel levelPanel;
    private final PlayGame playGame;
    private Font font;

    public StartPanel(PlayGame playGame) {
        this.img = new ImageIcon("resources/images/bg.jpg").getImage();
        this.width = playGame.getWidth();
        this.height = playGame.getHeight();
        this.levelPanel = playGame.getLevelPanel();
        this.startButton = new JButton("START");
        this.exitButton = new JButton("QUIT");
        this.continueButton = new JButton("CONTINUE");
        this.playGame = playGame;

        try {
            font = Font.createFont(Font.TRUETYPE_FONT, new File("resources/fonts/FFF_Tusj.ttf")).deriveFont(72f);
            GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
            graphicsEnvironment.registerFont(font);
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }

        startButton.setBounds(185, 150, 300, 81);
        initButton(startButton);

        continueButton.setBounds(100, 250, 500, 81);
        initButton(continueButton);

        exitButton.setBounds(185, 350, 300, 81);
        initButton(exitButton);

        setLayout(null);
        add(startButton);
        add(continueButton);
        add(exitButton);

        startButtonListener();
        continueButtonListener();
        exitButtonListener();
    }

    // initialisation of the buttons
    public void initButton(JButton button) {
        button.setForeground(Color.BLACK);
        button.setContentAreaFilled(false);
        button.setOpaque(false);
        button.setBorderPainted(false);
        button.setFont(font);

        button.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent evt) {
                button.setForeground(Color.RED);
            }
        });

        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                button.setForeground(Color.BLACK);
            }
        });
    }


    // start button listener
    public void startButtonListener() {
        startButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                playGame.getWindow().setPanel(levelPanel);
                playGame.getMyKeyListener().setReleaseAllKeys();
                playGame.getWindow().setLevelPanel(levelPanel);
                playGame.getWindow().requestFocus();
            }
        });
    }

    // continue button listener
    public void continueButtonListener() {
        continueButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int n = JOptionPane.showConfirmDialog(null, "Do you want to load the previous game or you want to continue another one? \nIf no: Please select the file which you want to load in.", "Info!", JOptionPane.YES_NO_OPTION);

                String filename;
                if (n == JOptionPane.YES_OPTION) {
                    filename = "C:\\Users\\varga\\Documents\\JAVA\\Projekt2\\levels\\previous.txt";
                    try {
                        Scanner scanner = new Scanner(new File(filename));
                        filename = scanner.nextLine();
                        scanner.close();
                        Scanner scanner1 = new Scanner(new File(filename));
                        playGame.setUnlocked(scanner1.nextInt());
                        playGame.setTotal(scanner1.nextInt());
                        scanner1.close();
                        levelPanel = new LevelPanel(playGame);
                        playGame.setLevelPanel(levelPanel);
                        Arrays.stream(levelPanel.getLevelButton()).filter(s -> Integer.parseInt(s.getText()) <= playGame.getUnlocked()).forEach(s -> s.setBackground(new Color(137, 207, 240)));
                        playGame.getLevelPanel().getDiamondsAccumulated().setText("Diamonds in total: " + playGame.getTotal());
                        playGame.getWindow().setPanel(levelPanel);
                        playGame.getMyKeyListener().setReleaseAllKeys();
                        playGame.getWindow().setLevelPanel(levelPanel);
                        playGame.getWindow().requestFocus();
                    } catch (FileNotFoundException fileNotFoundException) {
                        fileNotFoundException.printStackTrace();
                    }
                } else if (n == JOptionPane.NO_OPTION) {
                    JFileChooser fileChooser = new JFileChooser();
                    if (fileChooser.showOpenDialog(playGame.getWindow()) == JFileChooser.APPROVE_OPTION) {
                        File file = fileChooser.getSelectedFile();
                        try {
                            Scanner scanner = new Scanner(new File(file.getAbsolutePath()));
                            playGame.setUnlocked(scanner.nextInt());
                            playGame.setTotal(scanner.nextInt());
                            levelPanel = new LevelPanel(playGame);
                            playGame.setLevelPanel(levelPanel);
                            Arrays.stream(levelPanel.getLevelButton()).filter(s -> Integer.parseInt(s.getText()) <= playGame.getUnlocked()).forEach(s -> s.setBackground(new Color(137, 207, 240)));
                            playGame.getLevelPanel().getDiamondsAccumulated().setText("Diamonds in total: " + playGame.getTotal());
                            playGame.getWindow().setPanel(levelPanel);
                            playGame.getMyKeyListener().setReleaseAllKeys();
                            playGame.getWindow().setLevelPanel(levelPanel);
                            playGame.getWindow().requestFocus();
                        } catch (FileNotFoundException fileNotFoundException) {
                            fileNotFoundException.printStackTrace();
                        }
                    }
                } else if (n == JOptionPane.CLOSED_OPTION) {
                    System.exit(0);
                }
            }
        });
    }

    // quit button listener
    public void exitButtonListener() {
        exitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int n = JOptionPane.showConfirmDialog(null, "Do you want to save your progress?", "Info!", JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_OPTION) {
                    JFileChooser fileChooser = new JFileChooser();
                    if (fileChooser.showOpenDialog(playGame.getWindow()) == JFileChooser.APPROVE_OPTION) {
                        File file = fileChooser.getSelectedFile();

                        FileWriter fileWriter;
                        try {
                            fileWriter = new FileWriter(new File(file.getAbsolutePath()));
                            fileWriter.write(playGame.getUnlocked() + "\n" + playGame.getTotal());

                            try {
                                fileWriter.close();
                            } catch (IOException ioException) {
                                ioException.printStackTrace();
                            }

                            FileWriter previous = new FileWriter(new File("levels/previous.txt"));
                            previous.write(file.getAbsolutePath());
                            try {
                                previous.close();
                            } catch (IOException ioException) {
                                ioException.printStackTrace();
                            }

                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }
                    System.exit(0);
                } else if (n == JOptionPane.NO_OPTION || n == JOptionPane.CLOSED_OPTION) {
                    System.exit(0);
                }
            }
        });

    }



    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, width, height, null);
    }
}

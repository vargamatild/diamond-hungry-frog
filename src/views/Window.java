package views;

import elements.Sound;

import javax.swing.*;

public class Window extends JFrame {
    private final int width = 695;
    private final int height = 715;
    private JPanel panel;
    private JPanel levelPanel;
    private JPanel startPanel;
    private final JMenuBar menuBar = new JMenuBar();
    private final JMenu menu = new JMenu("Menu");
    private final JMenuItem backToLevels = new JMenuItem("Back to levels.");
    private final JMenuItem backToStart = new JMenuItem("Back to Menu.");
    private final JMenuItem volumeOff = new JMenuItem("Turn off the sound.");
    private final JMenuItem volumeOn = new JMenuItem("Turn on the sound.");
    private final JLabel score = new JLabel();

    public Window(JPanel panel) {
        this.panel = panel;

        setTitle("Diamond-hungry frog");
        setResizable(false);
        setVisible(true);
        setSize(width, height);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        score.setBounds(100, 100, 50, 50);
        Sound sound = new Sound("resources/sounds/sound.wav");
        sound.loop();

        menu.add(backToLevels);
        menu.add(backToStart);
        menu.add(volumeOff);
        menu.add(volumeOn);
        menuBar.add(menu);
        setJMenuBar(menuBar);
        backToLevels.addActionListener(e -> setPanel(levelPanel));
        backToStart.addActionListener(e -> setPanel(startPanel));
        volumeOff.addActionListener(e -> sound.stop());
        volumeOn.addActionListener(e -> sound.loop());

        add(this.panel);
    }


    // setting the panel, which will be painted
    public void setPanel(JPanel panel) {
        remove(this.panel);
        this.panel = panel;
        add(this.panel);
        repaint();
        revalidate();
    }

    public void setStartPanel(JPanel startPanel) {
        this.startPanel = startPanel;
    }

    public void setLevelPanel(JPanel levelPanel) {
        this.levelPanel = levelPanel;
    }

    public void setScoreText(String text) {
        this.score.setText(text);
    }
}
